import { Container, Row, Col, Input, Label, Button } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div>
      <Container>
        <Row className="text-center mt-3">
          <h3>Registration Form</h3>
        </Row>
        <Row className='mt-3'>
          <Col>
            First Name <span style={{ color: 'red' }}>(*)</span>
          </Col>
          <Col xs="4">
            <input className="form-control" />
          </Col>

          <Col>
            Passport <span style={{ color: 'red' }}>(*)</span>
          </Col>
          <Col xs="4">
            <input className="form-control" />
          </Col>
        </Row>
        <Row className='mt-3'>
          <Col>
            Last Name <span style={{ color: 'red' }}>(*)</span>
          </Col>
          <Col xs="4">
            <input className="form-control" />
          </Col>

          <Col>
            Email
          </Col>
          <Col xs="4">
            <input className="form-control" />
          </Col>
        </Row>
        <Row className='mt-3'>
          <Col>
            Birth Day <span style={{ color: 'red' }}>(*)</span>
          </Col>
          <Col xs="4">
            <input className="form-control" />
          </Col>

          <Col>
            Country <span style={{ color: 'red' }}>(*)</span>
          </Col>
          <Col xs="4">
            <input className="form-control" />
          </Col>
        </Row>
        <Row className='mt-3'>
          <Col>
            Gender <span style={{ color: 'red' }}>(*)</span>
          </Col>
          <Col xs="4">
            <Row>
              <Col xs="2">
                <Input type="checkbox" />
              </Col>
              <Col xs="4">
                <Label check>
                  Male
                </Label>
              </Col>
              <Col xs="2">
                <Input type="checkbox" />
              </Col>
              <Col xs="4">
                <Label check>
                  Female
                </Label>
              </Col>
            </Row>
          </Col>

          <Col>
            Passport <span style={{ color: 'red' }}>(*)</span>
          </Col>
          <Col xs="4">
            <input className="form-control" />
          </Col>
        </Row>
        <Row className="mt-3">
          <Col>
            Subject:
          </Col>
          <Col xs="10">
            <textarea rows="3" className="form-control"></textarea>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col style={{marginLeft:"600px"}}>
          <Button className="m-1" style={{width:"125px"}} color="success">
            Check Data
          </Button>
          <Button className="m-1" style={{width:"125px"}} color="success">
            Send
          </Button>
          </Col>          
        </Row>
      </Container>
    </div>
  );
}

export default App;
